export default class Raffle {
    private tickets;
    private ts;
    private rn;
    private hash;
    constructor(tickets: Array<any>);
    /**
     * Returns an array of shuffled tickets
     * @param tickets Array of tickets
     */
    GetShuffledTickets(): Array<any>;
    GetTicket(): any;
    GetTicketAudit(): any;
    GetLog(): {
        ver: string;
        tickets: any[];
        ticket: any;
        rn: number;
        ts: string;
        hash: any;
    };
    Validate(tickets: Array<any>, ts: string, hash: string): boolean;
    GetAudit(tickets: Array<any>, rn: number): any;
}
