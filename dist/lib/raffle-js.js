"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var shuffleSeed = require("shuffle-seed");
var md5_1 = require("ts-md5/dist/md5");
var Raffle = /** @class */ (function () {
    function Raffle(tickets) {
        this.tickets = tickets;
        this.rn = 0;
        this.ts = Math.floor(Date.now() / 1000).toString();
        this.hash = md5_1.Md5.hashStr(JSON.stringify(tickets) + this.ts);
    }
    /**
     * Returns an array of shuffled tickets
     * @param tickets Array of tickets
     */
    Raffle.prototype.GetShuffledTickets = function () {
        return shuffleSeed.shuffle(this.tickets, this.ts);
    };
    Raffle.prototype.GetTicket = function () {
        var shuffledTickets = this.GetShuffledTickets();
        this.rn = Math.random();
        return shuffledTickets[Math.floor(this.rn * this.tickets.length)];
    };
    Raffle.prototype.GetTicketAudit = function () {
        var shuffledTickets = this.GetShuffledTickets();
        return shuffledTickets[Math.floor(this.rn * this.tickets.length)];
    };
    Raffle.prototype.GetLog = function () {
        return {
            ver: '1.0',
            tickets: this.tickets,
            ticket: this.GetTicketAudit(),
            rn: this.rn,
            ts: this.ts,
            hash: this.hash
        };
    };
    Raffle.prototype.Validate = function (tickets, ts, hash) {
        var hashCheck = md5_1.Md5.hashStr(JSON.stringify(tickets) + ts);
        return hashCheck === hash;
    };
    Raffle.prototype.GetAudit = function (tickets, rn) {
        this.tickets = tickets;
        this.rn = rn;
        return this.GetTicketAudit();
    };
    return Raffle;
}());
exports.default = Raffle;
//# sourceMappingURL=raffle-js.js.map