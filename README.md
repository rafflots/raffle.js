# Raffle.js

Raffle alghorithm used by Rafflots. 

* Picks a winning ticket from a group of tickets
* Provides a log of results
* Provides the ability to audit results

![Winning Ticket probability](assets/raffle-js-stats.png "Winning ticket probability")

Rafflots (c) 2020. All rights reserved.