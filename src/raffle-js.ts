import * as shuffleSeed from 'shuffle-seed'
import { Md5 } from 'ts-md5/dist/md5'

export default class Raffle {
  
  private tickets: Array<any>
  private ts: string
  private rn: number
  private hash: any

  constructor(tickets: Array<any>) {
    this.tickets = tickets
    this.rn = 0
    this.ts = Math.floor(Date.now() / 1000).toString()
    this.hash = Md5.hashStr(JSON.stringify(tickets) + this.ts)
  }

  static get version() {
    return '1.0'
  }

  /**
   * Returns an array of shuffled tickets
   * @param tickets Array of tickets
   */
  public GetShuffledTickets(): Array<any> {
    return shuffleSeed.shuffle(this.tickets, this.ts)
  }

  public GetTicket() {
    const shuffledTickets = this.GetShuffledTickets()
    this.rn = Math.random()
    return shuffledTickets[Math.floor(this.rn * this.tickets.length)]
  }

  public GetTicketAudit() {
    const shuffledTickets = this.GetShuffledTickets()
    return shuffledTickets[Math.floor(this.rn * this.tickets.length)]
  }

  public GetLog() {
    return {
      ver: Raffle.version,
      tickets: this.tickets,
      ticket: this.GetTicketAudit(),
      rn: this.rn,
      ts: this.ts,
      hash: this.hash
    }
  }

  public Validate(tickets: Array<any>, ts: string, hash: string): boolean {
    let hashCheck = Md5.hashStr(JSON.stringify(tickets) + ts)
    return hashCheck === hash
  }

  public GetAudit(tickets: Array<any>, rn: number) {
    this.tickets = tickets
    this.rn = rn
    return this.GetTicketAudit()
  }
}
