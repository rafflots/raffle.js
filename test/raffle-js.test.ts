import Raffle from "../src/raffle-js"

/**
 * Raffle test
 */
describe("Raffle test", () => {
  it("Raffle class is instantiable", () => {
    expect(new Raffle(['a', 'b', 'c', 'd', 'e'])).toBeInstanceOf(Raffle)
  })

  it('Raffle should pass audit', () => {
    let tickets = ['a', 'b', 'c', 'd', 'e']
    let raffle = new Raffle(tickets)
    let ticket = raffle.GetTicket()
    let log = raffle.GetLog()

    expect(tickets).toContain(ticket)
    expect(log.ver).toBe('1.0')
    expect(raffle.Validate(log.tickets, log.ts, log.hash)).toBeTruthy()
    expect(raffle.GetAudit(log.tickets, log.rn)).toEqual(log.ticket)
  })
})
