const Raffle = require('../dist/lib/raffle-js').default
let count = 10
let tickets = []
let stats = {}

for (let i = 0; i < count; i++) {
  let ticket = "ticket" + i
  tickets.push(ticket)
  stats[ticket] = 0
}

for (let i = 0; i < 1000000; i++) {
  let raffle = new Raffle(tickets)
  let ticket = raffle.GetTicket()
  stats[ticket]++
}

console.log('stats', stats)
